package Advanced;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/*
    twórz metodę, która jako parametr przyjmuje mapę, gdzie kluczem jest string, a wartością liczba,
    a następnie wypisuje każdy element mapy do konsoli w formacie: Klucz: <k>, Wartość: <v>.
    Na końcu każdego wiersza poza ostatnim, powinien być przecinek, a w ostatnim kropka.
 */
public class Zadanie3 {
    public static void main(String[] args) {
        Map<String, Integer> map = new LinkedHashMap<>() {{
            put("first", 1);
            put("middle", 2);
            put("last", 3);
        }};

        printWithCommasAndDot(map);
    }

    public static void printWithCommasAndDot(Map<String, Integer> map) {
        int size = map.size();

        for (Map.Entry<String, Integer> pair : map.entrySet()) {
            if (size-- > 1) {
                System.out.println(pair.getKey() + ", " + pair.getValue() + ",");
            } else {
                System.out.println(pair.getKey() + ", " + pair.getValue() + ".");
            }
        }
    }

    public static void printWithCommasAndDotIterator(Map<String, Integer> map) {
        Iterator<Map.Entry<String, Integer>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Integer> entry = iterator.next();
            System.out.print(entry.getKey() + ", " + entry.getValue());
            if (iterator.hasNext()) {
                System.out.println(",");
            } else {
                System.out.println(".");
            }
        }
    }

}
