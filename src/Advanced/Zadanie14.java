package Advanced;

import java.util.*;
import java.util.stream.Collectors;

/*
    Na podstawie 30 elementowej tablicy z losowo wybranymi wartościami zaimplementuj następujące funkcjonalności:
    1. zwróć listę unikalnych elementów,
    2. zwróć listę elementów, które co najmniej raz powtórzyły się w wygenerowanej tablicy,
    3. zwróć listę 5 najczęściej powtarzających się elementów.

    Zaimplementuj metodę, która deduplikuje elementy w liście. W przypadku znalezienia duplikatu,
    zastępuje go nową losową wcześniej nie występującą wartością.
    Sprawdź czy metoda zadziałała poprawnie wywołując metodę numer 2.
 */
public class Zadanie14 {
    private static final int ARRAY_SIZE = 30;
    private static final int RANDOM_BOUND = 10;

    public static void main(String[] args) {
        int[] randomNumbers = generateRandomArray(ARRAY_SIZE, RANDOM_BOUND);
        displayArray(randomNumbers);

        // 1
        int[] distincts = getUniques(randomNumbers);
        System.out.print("Distincts: ");
        displayArray(distincts);

        // 2
        displayOccurrencesMoreThanOneTime(randomNumbers);

        // 3
        displayMostRepeatedOccurrences(randomNumbers);

    }

    // 1
    private static int[] getUniques(int[] array) {
        int[] uniques = Arrays.stream(array).distinct().toArray();
        return uniques;
    }

    // 2
    private static void displayOccurrencesMoreThanOneTime(int[] randomNumbers) {
        Map<Integer, Integer> occurences = new HashMap<>();

        for (Integer number : randomNumbers) {
            if (occurences.containsKey(number)) {
                int count = occurences.get(number);
                occurences.put(number, ++count);
            } else {
                occurences.put(number, 1);
            }
        }

        for (Map.Entry<Integer, Integer> pair : occurences.entrySet()) {
            if (pair.getValue() > 1) {
                System.out.println("Number " + pair.getKey() + " occurs more than one time");
            }
        }
    }

    // 3
    private static void displayMostRepeatedOccurrences(int[] randomNumbers) {
        Map<Integer, Integer> occurences = new HashMap<>();

        // count occurences
        for (Integer number : randomNumbers) {
            if (occurences.containsKey(number)) {
                int count = occurences.get(number);
                occurences.put(number, ++count);
            } else {
                occurences.put(number, 1);
            }
        }

        displayMap(occurences);

        // sort entry set descending
        List<Map.Entry<Integer, Integer>> collect = occurences.entrySet().stream()
                .sorted((o1, o2) -> Integer.compare(o2.getValue(), o1.getValue()))
//                .sorted(new Comparator<Map.Entry<Integer, Integer>>() {
//                    @Override
//                    public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {
//                        return Integer.compare(o2.getValue(), o1.getValue());
//                    }
//                })
                .collect(Collectors.toList());

        // display collection limiting output to 5
        for (int i = 0; i < collect.size() && i < 5; i++) {
            System.out.println(collect.get(i));
        }

    }

    // helper methods
    private static int[] generateRandomArray(int size, int bound) {
        int[] array = new int[size];
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(bound);
        }
        return array;
    }

    private static void displayArray(int[] array) {
        for (int ints : array) {
            System.out.print(ints + ", ");
        }
        System.out.println();
    }

    private static <T, K> void displayMap(Map<T, K> map) {
        for (Map.Entry<T, K> pair : map.entrySet()) {
            System.out.println("Number " + pair.getKey() + " occurs " + pair.getValue() + " times");
        }
        System.out.println();
    }

}
