package Advanced;

import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/*
    Stwórz metodę, która jako parametr przyjmuje listę stringów,
    następnie zwraca tą listę posortowaną alfabetycznie od Z do A.
 */
public class Zadanie1 {

    public static void main(String[] args) {
        List<String> stringsA = new LinkedList<>(Arrays.asList("wiola", "ola", "ala", "tola"));
        List<String> stringsB = new LinkedList<>(Arrays.asList("wiola", "ola", "ala", "tola"));

        List<String> sortedA = sortAlphabeticallyA(stringsA);
        sortedA.stream()
                .forEach(System.out::println);

        List<String> sortedB = sortAlphabeticallyB(stringsB);
        sortedB.stream()
                .forEach(System.out::println);

    }

    public static List<String> sortAlphabeticallyA(List<String> strings) {
        strings.sort(Comparator.naturalOrder());

        return strings;
    }

    public static List<String> sortAlphabeticallyB(List<String> strings) {
        return strings.stream()
                .sorted()
                .collect(Collectors.toList());
    }


}
