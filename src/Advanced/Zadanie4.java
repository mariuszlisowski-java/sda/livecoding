package Advanced;

import java.util.*;

/*
    Stwórz klasę Storage, która będzie miała prywatne pole typu Map, publiczny konstruktor oraz metody:
        addToStorage(String key, String value) → dodawanie elementów do przechowywalni
        printValues(String key) → wyświetlanie wszystkich elementów pod danym kluczem
        findValues(String value) → wyświetlanie wszystkich kluczy, które mają podaną wartość

    Klasa Storage powinna pozwalać na przechowywanie wielu wartości pod jednym kluczem.
 */

public class Zadanie4 {

    public static void main(String[] args) {
        Storage storage = new Storage();

        storage.addToStorage("1st", "one");
        storage.addToStorage("1st", "two");
        storage.addToStorage("1st", "three");

        storage.addToStorage("2nd", "three");
        storage.addToStorage("2nd", "four");
        storage.addToStorage("2nd", "five");
        storage.addToStorage("2nd", "six");

        storage.printValue("1st");
        storage.printValue("2nd");

        storage.findValues("three");
        storage.findValuesAlt("three");
    }

    public static class Storage implements IStorage {
        private Map<String, List<String>> map;
        private List<String> list;

        public Storage() {
            this.map = new LinkedHashMap<>();
        }

        public void addToStorage(String key, String value) {
            if (map.containsKey(key)) {
                list = map.get(key);
                list.add(value);
            } else {
                list = new ArrayList<>();
                list.add(value);
                map.put(key, list);
            }
        }

        public void printValue(String key) {
            list = map.get(key);
            System.out.println("Map of size: " + list.size());
            for (String string : list) {
                System.out.println(string);
            }
        }

        public void findValues(String value) {
            for (Map.Entry<String, List<String>> pair : map.entrySet()) {
                for (String string : pair.getValue()) {
                    if (string.equals(value)) {
                        System.out.println('\'' + value + '\'' + " found in key " + '\'' + pair.getKey() + '\'');
                    }
                }
            }
        }

        public void findValuesAlt(String value) {
            for (Map.Entry<String, List<String>> pair : map.entrySet()) {
                List<String> list = pair.getValue();
                Optional<String> first = list.stream()
                        .filter(text -> text.equals(value))
                        .findFirst();
                if (first.isPresent()) {
                    System.out.println('\'' + value + '\'' + " found in key " + '\'' + pair.getKey() + '\'');
                }
            }
        }

    }

    interface IStorage {
        void addToStorage(String key, String value);
        void printValue(String key);
        void findValues(String value);
    }

}
