package Advanced;

public class Zadanie7 {

    public static void main(String[] args) {
        Cardridge cardridge = new Cardridge(10);

        cardridge.loadBullet(1);
        cardridge.loadBullet(1);
        cardridge.shot();
        cardridge.shot();
        cardridge.shot();

        if (cardridge.isLoaded()) {
            System.out.println("Cartridge current load: " +
                    cardridge.currntBulletCount +
                    " / " + cardridge.CARDRIDGE_CAPACITY);
        } else {
            System.out.println("Cardridge empty!");
        }

    }

    public static class Cardridge {
        private final int CARDRIDGE_CAPACITY;
        private int currntBulletCount = 0;

        public Cardridge(int cardridgeCapacity) {
            CARDRIDGE_CAPACITY = cardridgeCapacity;
        }
        public void loadBullet(int bullets) {
            if (currntBulletCount < CARDRIDGE_CAPACITY) {
                if (currntBulletCount + bullets <= CARDRIDGE_CAPACITY) {
                    System.out.println(bullets + " bullet(s) added");
                    currntBulletCount += bullets;
                } else {
                    System.out.println("Too many to add! Max to be added: " +
                            (CARDRIDGE_CAPACITY - currntBulletCount));
                }
            } else {
                System.out.println("Cardridge already full");
            }
        }

        public boolean isLoaded() {
            return currntBulletCount > 0;
        }

        public void shot() {
            if (currntBulletCount > 0) {
                System.out.println("Shooting one bullet...");
                --currntBulletCount;
            } else {
                System.out.println("Sorry, no bullets left");
            }
        }
    }
}
