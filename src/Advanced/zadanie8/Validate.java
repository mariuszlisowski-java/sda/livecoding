package Advanced.zadanie8;

public class Validate implements IValidate {
    private static final int MAX_LENGTH_SIDE = 30;
    private static final int MAX_LENGTH_ALL = 300;
    private static final int MAX_WEIGHT_REGULAR = 30;
    private static final int MAX_WEIGHT_EXPRESS = 15;

    @Override
    public boolean validate(Parcel parcel) {
        if (parcel.x + parcel.y + parcel.z > 300) {
            throw new ParcelTooLongException();
        }
        if (parcel.x > MAX_WEIGHT_REGULAR || parcel.y > MAX_WEIGHT_REGULAR || parcel.z > MAX_WEIGHT_REGULAR) {
            throw new ParcelSizeTooLongException();
        }
        if (parcel.weight > 30) {
            throw new ParcelTooHeavyException();
        }
        if (parcel.isExpress && parcel.weight > MAX_WEIGHT_EXPRESS) {
            throw new ParcelTooHeavyExpressException();
        }

        return true;
    }
}
