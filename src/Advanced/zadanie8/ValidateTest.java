package Advanced.zadanie8;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ValidateTest {
    private Validate validate = new Validate();

    @Test
    public void sidesBiggerThan300() {
        Parcel parcel = new Parcel(300, 10, 10, 2, false);

        assertThrows(ParcelTooLongException.class, () -> validate.validate(parcel));
    }

    @Test
    public void sidesTooLong() {
        Parcel parcel = new Parcel(31, 31, 29, 10, false);

        assertThrows(ParcelSizeTooLongException.class, () -> validate.validate(parcel));
    }

    @Test
    public void tooHeavy() {
        Parcel parcel = new Parcel(29, 29, 29, 31, false);

        assertThrows(ParcelTooHeavyException.class, () -> validate.validate(parcel));
    }

    @Test
    public void tooHeavyForExpress() {
        Parcel parcel = new Parcel(29, 29, 29, 16, true);

        assertThrows(ParcelTooHeavyExpressException.class, () -> validate.validate(parcel));

    }
}