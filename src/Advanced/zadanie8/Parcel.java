package Advanced.zadanie8;


public class Parcel {
    int x, y, z;
    float weight;
    boolean isExpress;

    public Parcel(int x, int y, int z, float weight, boolean isExpress) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.weight = weight;
        this.isExpress = isExpress;
    }

}
