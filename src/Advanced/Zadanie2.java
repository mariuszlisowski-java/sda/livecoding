package Advanced;

import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

/*
    Stwórz metodę, która jako parametr przyjmuje listę stringów, następnie zwraca tą listę posortowaną
    alfabetycznie od Z do A nie biorąc pod uwagę wielkości liter.
 */

public class Zadanie2 {

    public static void main(String[] args) {
//        List<String> stringsA = new LinkedList<>(Arrays.asList("wiola", "ola", "Ala", "Tola"));
        List<String> stringsA = new LinkedList<>(Arrays.asList("wiola", "ola", "Ala", "Tola"));
        List<String> stringsB = new LinkedList<>(Arrays.asList("wiola", "ola", "Ala", "Tola"));

        // case sensitive
        List<String> sortedA = sortAlphabeticallyCaseSensitive(stringsA);
        sortedA.stream()
                .forEach(System.out::println);

        System.out.println();

        // case insensitive (unwanted here)
        List<String> sortedB = sortAlphabeticallyCaseInsensitive(stringsB);
        sortedB.stream()
                .forEach(System.out::println);

    }

    public static List<String> sortAlphabeticallyCaseSensitive(List<String> strings) {
        strings.sort(Comparator.comparing(String::toLowerCase, String::compareTo));

        return strings;
    }

    public static List<String> sortAlphabeticallyCaseInsensitive(List<String> strings) {
        strings.sort(Comparator.naturalOrder());

        return strings;
    }


}
