package Advanced;

import java.util.Iterator;
import java.util.Optional;
import java.util.TreeSet;

/*
    Zaimplementuj klasę SDAHashSet<E> , która będzie implementować logikę HashSet<E>. W tym celu
        zaimplementuj obsługę metod:
        • add
        • remove • size
        • contains • clear
 */
public class Zadanie6 {

    public static void main(String[] args) {
        TreeSet<Integer> treeSet = new TreeSet<>();

        Optional<Integer> first = getFirst(treeSet);
        Optional<Integer> last = getLast(treeSet);
    }

    private static Optional<Integer> getLast(TreeSet<Integer> treeSet) {
        Iterator<Integer> iterator = treeSet.iterator();

        while (iterator.hasNext()) {
            Integer next = iterator.next();
            if (!iterator.hasNext()) {
                return Optional.of(next);
            }
        }

        return Optional.empty();
    }

    private static Optional<Integer> getFirst(TreeSet<Integer> treeSet) {
        Iterator<Integer> iterator = treeSet.iterator();
        if (iterator.hasNext()) {

            Integer next = iterator.next();
            return Optional.of(next);
        }

        return Optional.empty();
    }


}
