package Elementary;

public class Zadanie10 {
    public static void main(String[] args) {
        System.out.println(sumOfDigits(123456)); // 21
    }

    public static int sumOfDigits(int number) {
        int sum = 0;
        while (number > 0) {
            int digit = number % 10;
            sum += digit;
            number /= 10;
        }

        return sum;
    }

}
