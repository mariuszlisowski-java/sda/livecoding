package Elementary;

import java.sql.SQLType;
import java.util.Random;
import java.util.Scanner;

public class Zadanie20 {
    public static void main(String[] args) {
        guessGame();
    }

    public static void guessGame() {
        Random random = new Random();
        Scanner scanner = new Scanner(System.in);

        final int UPPERBOUND = 100;
        int randomNumber = random.nextInt(UPPERBOUND);

        while (true) {
            System.out.print("Guess a number (0-" + (UPPERBOUND - 1) + "): ");
            int guessed = scanner.nextInt();
            if (guessed < randomNumber) {
                System.out.println("Too small... try again!");
            } else if (guessed > randomNumber) {
                System.out.println("Too large... try again!");
            } else {
                System.out.println("Bingo!");
                break;
            }
        }
    }

}
