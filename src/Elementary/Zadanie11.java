package Elementary;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Zadanie11 {
    public static void main(String[] args) {
        try {
            printLongestSentence();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void printLongestSentence() throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String sentence;
        String longestSentence = "";
        System.out.println("Enter few sentences (until 'quit')");
        do {
            System.out.print(": ");
            sentence = bufferedReader.readLine();
            if (sentence.length() > longestSentence.length() &&
                !sentence.equals("quit")) {
                longestSentence = sentence;
            }
        } while (!sentence.equals("quit"));
        if (!longestSentence.equals("")) {
            System.out.println("The longest sentence: " + longestSentence);
        }
    }

}
