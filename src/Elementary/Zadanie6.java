package Elementary;// harmonic series sum

import java.util.Scanner;

public class Zadanie6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Harmonic series sum. Enter max series: ");
        System.out.println(harmonicSeries(scanner.nextInt()));
    }

    public static double harmonicSeries(int n) {
        // 1 + 1/2 + 1/3 + 1/4 + 1/n
        double sum = 1;
        for (int i = 2; i <= n; i++) {
            sum += 1d/i;
        }
        return sum;
    }

}
