package Elementary;

public class Zadanie5 {
    public static void main(String[] args) {
        primeNumbersLessThan(60);
    }

    // 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59
    public static void primeNumbersLessThan(int n) {
        System.out.print(2 + ", ");
        boolean status = true;
        // check all range [3; n)
        for (int i = 3; i < n; i++) {
            // even numbers > 2 are not prime
            if ((i & 1) != 0) { // go on if odd number
                for (int j = 2; j <= Math.sqrt(i); j++) {
                    // if divisible by other than itself
                    if (i % j == 0) {
                        status = false;
                        break;
                    }
                }
                // divisible only by 1 and itself thus prime
                if (status != false) {
                    System.out.print(i + ", ");
                }
                status = true;
            }
        }

    }

}
