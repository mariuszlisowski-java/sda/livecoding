package Elementary.Zadanie19;

public class Main {
    public static void main(String[] args) {
        Poem[] poems = new Poem[3];

        Author famousAuthor = new Author("Asimov", "USA");
        Author unknownAuthor = new Author("Anonym", "N/A");
        Author otherAuthor = new Author("Green", "USA");

        Poem beautifulPoem = new Poem(famousAuthor, 99);
        Poem stunningPoem = new Poem(unknownAuthor, 29);
        Poem marvelousPoem = new Poem(otherAuthor, 19);

        poems[0] = beautifulPoem;
        poems[1] = stunningPoem;
        poems[2] = marvelousPoem;

        System.out.println(mostProlificAuthor(poems));
    }

    public static Author mostProlificAuthor(Poem[] poems) {
        int maxStrophes = 0, index = 0;
        for (int i = 0; i < 3; i++) {
            int strophes = poems[i].getStropheNumbers();
            if (strophes > maxStrophes) {
                maxStrophes = strophes;
                index = i;
            }
        }

        return poems[index].getCreator();
    }

}
