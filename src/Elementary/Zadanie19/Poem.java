package Elementary.Zadanie19;

public class Poem {
    public Poem(Author creator) {
        this.creator = creator;
    }

    public Poem(Author creator, int stropheNumbers) {
        this.creator = creator;
        this.stropheNumbers = stropheNumbers;
    }

    public int getStropheNumbers() {
        return stropheNumbers;
    }
    public Author getCreator() {
        return creator;
    }

    private Author creator;
    private int stropheNumbers;
}
