package Elementary.Zadanie19;

public class Author {
    public Author(String surname, String nationality) {
        this.surname = surname;
        this.nationality = nationality;
    }

    public String toString() {
        return "Author' name: " + surname +
               "\nAuthor's nationality: " + nationality;
    }

    private String surname = "";
    private String nationality = "";
}
