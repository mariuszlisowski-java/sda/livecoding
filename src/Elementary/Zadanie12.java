package Elementary;

public class Zadanie12 {
    final static int PERCENT = 100;

    public static void main(String[] args) {
        System.out.println("Percent of spaces in the sentence: " +
                           spacesOccurencesCounter("a a a a "));
    }

    public static double spacesOccurencesCounter(String sentence) {
        long spacesNumber = sentence.chars().filter(ch -> ch == ' ').count();

        return (double)spacesNumber / sentence.length() * PERCENT;
    }

}
