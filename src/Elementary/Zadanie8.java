package Elementary;

import java.security.InvalidParameterException;
import java.util.Scanner;

public class Zadanie8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("First operand: ");
        float lhs = scanner.nextFloat();
        System.out.print("Operator (+ | - | * | /): ");
        char operator = scanner.next().charAt(0);
        System.out.print("Second operand: ");
        float rhs = scanner.nextFloat();

        try {
            System.out.println(calculate(lhs, rhs, operator));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static float calculate(float x, float y, char operator) throws Exception {
        switch (operator) {
            case '+':
                return x + y;
            case '-':
                return x - y;
            case '*':
                return x * y;
            case '/':
                if (y != 0) {
                    return x / y;
                } else {
                    throw new InvalidParameterException("Divide by zero not allowed");
                }
            default:
                throw new InvalidParameterException("Invalid operator");
        }
    }

}
