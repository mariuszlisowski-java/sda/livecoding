package Elementary;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class Zadanie17 {
    public static void main(String[] args) throws IOException, ParseException {
        InputStreamReader input = new InputStreamReader(System.in);
        BufferedReader reader = new BufferedReader(input);

        System.out.print("Enter date of the event (dd/mm/yyyy): ");
        String dateString = reader.readLine();

        LocalDate event = stringToLocalDate(dateString);

        System.out.println("Date of the event: " + event);
        daysToTheEvent(event);
        daysToTheEventChrono(event);
    }

    public static LocalDate stringToLocalDate(String dateString) throws ParseException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate localDate = LocalDate.parse(dateString, formatter);

        return localDate;
    }

    public static void daysToTheEvent(LocalDate event) {
        Period period = Period.between(LocalDate.now(), event);

        System.out.println("Days to the event: " + period.getDays());
    }

    public static void daysToTheEventChrono(LocalDate event) {
        final long daysElapsed = ChronoUnit.DAYS.between(LocalDate.now(), event);

        System.out.println("Days to the event: " + daysElapsed);
    }

}
