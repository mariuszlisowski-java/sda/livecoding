package Elementary;

import java.util.Scanner;

public class Zadanie1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);


        System.out.print("Enter circle diameter to obtain its circumference: ");
        System.out.println(circleCircumference(scanner.nextFloat()));
    }

    public static double circleCircumference(float diameter) {
        return diameter * Math.PI;
    }
}
