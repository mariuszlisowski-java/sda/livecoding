package Elementary;

public class Zadanie14 {
    public static void main(String[] args) {
        int distance = distanceBetweenChars('a', 'z'); // |97 - 122|

        System.out.println(distance);
    }

    public static int distanceBetweenChars(char first, char second) {
        return Math.abs((int)first - (int)second);
    }

}
