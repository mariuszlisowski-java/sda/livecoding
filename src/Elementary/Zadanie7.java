package Elementary;

public class Zadanie7 {
    public static void main(String[] args) {
        fibonacciSequence(16);
    }

    // 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377...
    public static void fibonacciSequence(int sequenceNumber) {
        int first = 0, second = 1;

        System.out.print(first + ", " + second + ", ");
        for (int i = 3; i < sequenceNumber; i++) {
            int fibonacciNo = first + second;
            first =  second;
            second = fibonacciNo;
            System.out.print(fibonacciNo + ", ");
        }
    }
}
