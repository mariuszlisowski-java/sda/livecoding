package Elementary;

public class Zadanie4 {
    public static void main(String[] args) {
        replaceNumbers(21);
    }

    public static void replaceNumbers(int n) {
        for (int i = 1; i <= n; i++) {
            if (i % 3 == 0 && i % 7 == 0) {
                System.out.println("pif paf");
            } else if (i % 7 == 0) {
                System.out.println("paf");
            } else if (i % 3 == 0) {
                System.out.println("pif");
            } else {
                System.out.println(i);
            }
        }
    }

}
