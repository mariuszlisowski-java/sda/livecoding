package Elementary;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Zadanie2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter your weight (kg): ");
        float weight = scanner.nextFloat();
        System.out.print("Enter your height (cm): ");
        int height = scanner.nextInt();

        calculateBodyMassIndex(weight, height);
    }

    public static void calculateBodyMassIndex(float weight, int height) {
        double bmi = weight / Math.pow(height/100d, 2);

        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        var bmiFormatted = decimalFormat.format(bmi);

        if (bmi >= 18.5 && bmi <= 24.9){
            System.out.println("BMI optimal (" + bmiFormatted + ")");
        } else {
            System.out.println("BMI not optimal (" + bmiFormatted + ")");
        }
    }

}
