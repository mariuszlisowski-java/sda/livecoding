package Elementary;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Zadanie18 {
    public static void main(String[] args) {
        String input = "Ala ma 'aaaaa psik' kota";

        System.out.println(dectectSubstring(input) ? "found" : "not found");
    }

    public static boolean dectectSubstring(String sencence) {
        Pattern sneeze = Pattern.compile("[^a]+psik");
        Matcher hasSneeze = sneeze.matcher(sencence);

        return hasSneeze.find();
    }

}
