package Elementary;

import java.util.Scanner;

public class Zadanie16 {
    final static int ARRAY_SIZE = 2;

    public static void main(String[] args) {
//        int[] array = readNumbersToArray();
        int[] array = {1, 3, 8, 4, 2, 5, 6, 11, 13, 7}; // 2, 5, 6, 11, 13
        findAscendingSubarray(array);
    }

    public static int[] readNumbersToArray() {
        Scanner scanner = new Scanner(System.in);
        int[] numbers = new int[ARRAY_SIZE];

        System.out.println("Enter integers...");
        for (int i = 0; i < ARRAY_SIZE; ++i) {
            System.out.print(i+1 + "/" + ARRAY_SIZE + ": ");
            numbers[i] = scanner.nextInt();
        }

        return numbers;
    }

    public static void findAscendingSubarray(int[] array) {
        int counter = 1, max = 0;
        for (int i = 0; i < array.length-1; i++) {
//            System.out.print(array[i] + " : " + array[i+1]);
            if (array[i] < array[i+1]) { // or <= for not descending
                counter++;
//                System.out.println(" - " + counter);
                if (counter > max) {
                    max = counter;
                }
            } else {
//                System.out.println();
                counter = 1;
            }
        }
        System.out.println("Number of elements of the longest ascending subarray: " + max);
    }

}
