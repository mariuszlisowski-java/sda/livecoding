package Elementary;// quadratic equation solver
// delta > 0 for a=-2, b=3, c=-1 (solution 1, 0.5)
// delta = 0 for a=4, b=4, c=1 (solution -0.5)
// delta < 0 for a=1, b=2, c=4 (no solution)

import java.util.Scanner;

public class Zadanie3 {
    public enum Delta {
        POSITIVE,
        ZERO
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("a*x^2 + b*x + c = 0");
        System.out.print("Enter 'a' factor: ");
        double a = scanner.nextDouble();
        System.out.print("Enter 'b' factor: ");
        double b = scanner.nextDouble();
        System.out.print("Enter 'c' factor: ");
        double c = scanner.nextDouble();

        try {
            Delta delta = quadraticEquation(a, b, c);
            System.out.println("Equation solution: ");
            if (delta == Delta.ZERO) {
                System.out.println("x = " + QuadraticSolution.x1);
            } else {
                System.out.println("x1 = " + QuadraticSolution.x1);
                System.out.println("x2 = " + QuadraticSolution.x2);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static class QuadraticSolution {
        public static double x1;
        public static double x2;
    }

    public static Delta quadraticEquation(double a, double b, double c) throws Exception{
        double delta = b*b - 4*a*c;

        if (delta < 0) {
            throw new IllegalStateException("Equation cannot be calculated (negative delta)");
        }
        if (delta == 0 ) {
            QuadraticSolution.x1 = -b / (2*a);
            return Delta.ZERO;
        }

        QuadraticSolution.x1 = (-b - Math.sqrt(delta)) / (2 * a);
        QuadraticSolution.x2 = (-b + Math.sqrt(delta)) / (2 * a);

        return Delta.POSITIVE;
    }
}
