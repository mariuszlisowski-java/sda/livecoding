package Elementary;

public class Zadanie13 {
    public static void main(String[] args) {
        System.out.println(doubleWordsInSentence("Ala ma kota"));
    }

    public static String doubleWordsInSentence(String sentence) {
        String[] words = sentence.split(" ");
        String doubbledWords = "";

        for (String word : words) {
            doubbledWords += word + " " + word + " ";
        }

        return doubbledWords;
    }

}
