package Elementary;

import java.util.stream.Stream;

public class Zadanie9 {
    public static void main(String[] args) {
        drawWave(4);
    }

    public static void drawWave(int length) {
        int side = 0;
        int middle = 6;
        String str = " ";

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < length; j++) {
                Stream.generate(() -> str).limit(side).forEach(System.out::print);
                System.out.print('*');
                Stream.generate(() -> str).limit(middle).forEach(System.out::print);
                System.out.print('*');
                Stream.generate(() -> str).limit(side).forEach(System.out::print);
            }
            System.out.println();
            side++;
            middle -= 2;
        }
    }

}
