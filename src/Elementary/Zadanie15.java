package Elementary;

import java.util.Arrays;
import java.util.Scanner;

public class Zadanie15 {
    final static short ARRAY_SIZE = 5;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] array = new int[ARRAY_SIZE];

        for (int i = 0; i < ARRAY_SIZE; i++) {
            System.out.print(i+1 +": ");
            array[i] = scanner.nextInt();
        }
        findDoubledNumbers(array);
    }

    public static int[] findDoubledNumbers(int[] numbers) {
        int[] sorted = Arrays.stream(numbers).sorted().toArray();
        int doubled = 0;
        boolean found = false;

        for (int i = 0; i < sorted.length - 1; ++i) {
            if (sorted[i] == sorted[i+1]) {
                doubled = sorted[i];
                found = true;
            }
        }

        if (found) {
            System.out.println("Doubled number: " + doubled);
        }

        return sorted;
    }

}
